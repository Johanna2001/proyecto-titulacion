// Obtenemos la referencia al elemento donde se mostrará la tabla
var tablaRegistros = document.getElementById("tablaRegistros");

// Obtenemos los registros almacenados en localStorage
var registros = JSON.parse(localStorage.getItem("registros"));

// Creamos la tabla de visualización de registros
if (registros && registros.length > 0) {
	var tabla = "<table>";
	tabla += "<tr><th>Nombre</th><th>Tema</th><th>Tutor</th><th>Tipo</th><th>Carrera</th></tr>";

	for (var i = 0; i < registros.length; i++) {
		var registro = registros[i];

		tabla += "<tr>";
		tabla += "<td>" + registro.nombre + "</td>";
		tabla += "<td>" + registro.tema + "</td>";
		tabla += "<td>" + registro.tutor + "</td>";
		tabla += "<td>" + registro.tipo + "</td>";
		tabla += "<td>" + registro.carrera + "</td>";
		tabla += "<td><button onclick='editarRegistro(" + i + ")'>Editar</button></td>";
        tabla += "</tr>";	
	}

	tabla += "</table>";

	// Agregamos la tabla a la página
	tablaRegistros.innerHTML = tabla;
	function editarRegistro(indice) {
		var registros = JSON.parse(localStorage.getItem("registros"));
		var registro = registros[indice];
		
		// Mostrar los valores del registro en campos de formulario
		document.getElementById("nombre").value = registro.nombre;
		document.getElementById("tema").value = registro.tema;
		document.getElementById("tutor").value = registro.tutor;
		document.getElementById("tipo").value = registro.tipo;
		document.getElementById("carrera").value = registro.carrera;
		
		// Guardar el índice del registro a editar en un campo oculto
		document.getElementById("indice").value = indice;
		
		// Cambiar el botón "Editar" por uno que permita guardar los cambios
		document.getElementById("boton-guardar").style.display = "inline-block";
		document.getElementById("boton-editar").style.display = "none";
	  }
	  function guardarRegistro() {
		var registros = JSON.parse(localStorage.getItem("registros"));
		var indice = document.getElementById("indice").value;
	
		// Actualizar el registro correspondiente en el array de registros
		registros[indice].nombre = document.getElementById("nombre").value;
		registros[indice].tema = document.getElementById("tema").value;
		registros[indice].tutor = document.getElementById("tutor").value;
		registros[indice].tipo = document.getElementById("tipo").value;
		registros[indice].carrera = document.getElementById("carrera").value;
	
		// Volver a guardar el array actualizado en localStorage
		localStorage.setItem("registros", JSON.stringify(registros));
	
		// Ocultar el botón "Guardar" y volver a mostrar el botón "Editar"
		document.getElementById("boton-guardar").style.display = "none";
		document.getElementById("boton-editar").style.display = "inline-block";
	
		// Actualizar la tabla de visualización de registros
		var tabla = "<table>";
		tabla += "<tr><th>Nombre</th><th>Tema</th><th>Tutor</th><th>Tipo</th><th>Carrera</th><th>Acciones</th></tr>";
	
		for (var i = 0; i < registros.length; i++) {
			var registro = registros[i];
	
			tabla += "<tr>";
			tabla += "<td>" + registro.nombre + "</td>";
			tabla += "<td>" + registro.tema + "</td>";
			tabla += "<td>" + registro.tutor + "</td>";
			tabla += "<td>" + registro.tipo + "</td>";
			tabla += "<td>" + registro.carrera + "</td>";
			tabla += "<td><button onclick='editarRegistro(" + i + ")'>Editar</button> <button onclick='eliminarRegistro(" + i + ")'>Eliminar</button></td>";
			tabla += "</tr>";
		}
	
		tabla += "</table>";
	
		document.getElementById("tablaRegistros").innerHTML = tabla;
	}
}
else {
	tablaRegistros.innerHTML = "No hay registros para mostrar.";
}
