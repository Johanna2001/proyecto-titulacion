const formulario = document.getElementById("formularioSI");
const tablaRegistros = document.getElementById("tablaRegistros");

formulario.addEventListener("submit", function(event) {
	event.preventDefault();

	const nombre = document.getElementById("nombre").value;
	const tema = document.getElementById("tema").value;
	const tutor = document.getElementById("tutor").value;
	const tipo = document.getElementById("tipo").value;

	// Obtenemos la lista de registros existentes del objeto localStorage (si es que existe)
	let registros = JSON.parse(localStorage.getItem("registros")) || [];

	// Creamos un objeto con los datos del formulario
	const registro = {
		nombre: nombre,
		tema: tema,
		tutor: tutor,
		tipo: tipo,
		carrera: "Ingeniería en SI"
	};

	// Agregamos el objeto al arreglo de registros
	registros.push(registro);

	// Guardamos el arreglo actualizado en el objeto localStorage
	localStorage.setItem("registros", JSON.stringify(registros));

	alert("La titulación ha sido registrada exitosamente.");
	formulario.reset();
});

// Cargamos la lista de registros del objeto localStorage al cargar la página
window.addEventListener("load", function() {
	let registros = JSON.parse(localStorage.getItem("registros")) || [];

	if (registros.length > 0) {
		// Creamos una tabla para mostrar los registros
		let tabla = "<table><tr><th>Nombre</th><th>Tema</th><th>Tutor</th><th>Tipo</th><th>Carrera</th></tr>";

		// Iteramos sobre la lista de registros y creamos una fila de la tabla para cada registro
		for (let registro of registros) {
			tabla += "<tr>";
			tabla += "<td>" + registro.nombre + "</td>";
			tabla += "<td>" + registro.tema + "</td>";
            tabla += "<td>" + registro.tutor + "</td>";
			tabla += "<td>" + registro.tipo + "</td>";
			tabla += "<td>" + registro.carrera + "</td>";
			tabla += "</tr>";
		}

		tabla += "</table>";

		// Agregamos la tabla a la página
		tablaRegistros.innerHTML = tabla;
	}
	else {
		tablaRegistros.innerHTML = "No hay registros para mostrar.";
	}
});
